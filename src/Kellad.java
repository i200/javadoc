/*
 * Selles failis tuleb igal pool asendada kaldkriipsudest koosnevad read
 * sobiliku javadoc kommentaariga. Seej2rel genereerida javadoc ja
 * kontrollida, et poleks t2itmata j22nud lahtreid.
 */

////////////////////////////////////////////////////
public class Kellad { // peameetodit sisaldav silumisklass
   
   ////////////////////////////////////////////////
   public static void main (String[] s) {
      Kell k = new KaeKell ("Rolex");
      System.out.println (((KaeKell)k).mark 
         + " " + k.jooksevAeg());
      Mobla m = new Mobla (25612345);
      System.out.println (String.valueOf (m.number)
         + " " + m.jooksevAeg());
   } // main

} // Kellad

///////////////////////////////////////////////
interface Ajanaitaja { // liides, mis kirjeldab oskust aega n2idata

   //////////////////////////
   String jooksevAeg(); // tagastab jooksva kellaaja stringina

} // Ajanaitaja

////////////////////////////////////////////
class Kell implements Ajanaitaja {

   ///////////////////////////////////////////
   public String jooksevAeg() {
      return new java.util.Date().toString();
   } // jooksevAeg

} // Kell

////////////////////////////////////////////
class KaeKell extends Kell {

   //////////////////////
   String mark; // kaekelladel peame meeles ka marki

   /////////////////////////////
   KaeKell (String s) {
      mark = s;
   } // konstruktor

} // KaeKell

///////////////////////////////////////////
class Telefon {

   //////////////////
   int number; // telefoninumber

   //////////////////////
   Telefon (int n) {
      number = n;
   } // konstruktor

} // Telefon

////////////////////////////////////////////////////
class Mobla extends Telefon implements Ajanaitaja {

   ///////////////////////
   private Kell sisemineKell; // moblasse sisseehitatud kell

   //////////////////////
   Mobla (int n) {
      super (n);
      sisemineKell = new Kell();
   } // konstruktor

   /////////////////////////////
   public String jooksevAeg() {
      return sisemineKell.jooksevAeg();
   } // jooksevAeg

} // Mobla

// faili l6pp

